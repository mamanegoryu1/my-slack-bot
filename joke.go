/*

Author: Roman Savchenko <r.sav4enko@gmail.com>
Created: Feb 28, 2017

*/

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type J struct {
	Type  string `json:"type"`
	Value struct {
		ID   int
		Joke string
	}
}

func getJoke(body []byte) (*J, error) {
	var s = new(J)
	err := json.Unmarshal(body, &s)
	if err != nil {
		fmt.Println("whoops:", err)
	}
	return s, err
}

func randomJoke() string {
	res, err := http.Get("http://api.icndb.com/jokes/random")
	if err != nil {
		panic(err.Error())
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err.Error())
	}

	s, err := getJoke([]byte(body))

	return s.Value.Joke
}
