/*

Author: Roman Savchenko <r.sav4enko@gmail.com>
Created: Feb 28, 2017

*/

package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"
	// _ "local/mybot/slack.go"
)

type Val struct {
	id   int
	joke string
}

var myClient = &http.Client{Timeout: 10 * time.Second}

func getJson(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

type jsonJoke struct {
	//{"type": "success", "value": { "id": 78, "joke": "The grass is always greener on the other side, unless Chuck Norris has been there. In that case the grass is most likely soaked in blood and tears.", "categories": [] } }
	// value []struct {
	// 	id   int
	// 	joke string
	// }
	value Val
}

// type WebKeys struct {
// 	Keys []struct {
// 		X5t string
// 		X5c []string
// 	}
// }

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "usage: mybot slack-bot-token\n")
		os.Exit(1)
	}

	// start a websocket-based Real Time API session
	ws, id := slackConnect(os.Args[1])
	fmt.Println("mybot ready, ^C exits")

	for {
		// read each incoming message
		m, err := getMessage(ws)
		if err != nil {
			log.Fatal(err)
		}

		// see if we're mentioned
		//if m.Type == "message" && strings.HasPrefix(m.Text, "<@"+id+">") && m.Channel == "D4H4JBPS6" {
		if m.Type == "message" && strings.HasPrefix(m.Text, "<@"+id+">") {
			// if so try to parse if

			parts := strings.Fields(m.Text)
			if len(parts) == 3 && parts[1] == "stock" {
				// looks good, get the quote and reply with the result
				go func(m Message) {
					m.Text = getQuote(parts[2])
					postMessage(ws, m)
				}(m)
				// NOTE: the Message object is copied, this is intentional
			} else if len(parts) == 3 && parts[1] == "say" {
				go func(m Message) {
					m.Text = parts[2]
					postMessage(ws, m)
				}(m)
			} else if len(parts) >= 2 && parts[1] == "joke" {
				go func(m Message) {
					m.Text = randomJoke()
					postMessage(ws, m)
				}(m)
			} else if len(parts) >= 2 && parts[1] == "get" && parts[2] == "last" && parts[3] == "point" {
				go func(m Message) {
					out, err := exec.Command("/bin/sh", "runGetLastPoint.sh").Output()
					if err != nil {
						// panic(err)
						log.Fatal(err)
					}
					response := parse_response(string(out))
					m.Text = response
					postMessage(ws, m)
				}(m)
			} else if len(parts) >= 2 && parts[1] == "make" && parts[2] == "in" {
				go func(m Message) {
					out, err := exec.Command("/bin/sh", "runMakeArrived.sh").Output()
					if err != nil {
						log.Fatal(err)
						return
					}
					r, _ := regexp.Compile(string("<response>([.*])</response>"))
					resp := r.FindAllString(string(out), 1)
					fmt.Printf("output is %s\n", resp)
					if resp != nil {
						m.Text = resp[0]
						postMessage(ws, m)
					}
				}(m)
			} else if len(parts) >= 2 && parts[1] == "make" && parts[2] == "out" {
				// (parts[2] == "out" || parts[2] == "live" || parts[2] == "left") {
				go func(m Message) {
					// out, err := exec.Command("sh ~/work/microbilt_bot/runGetLastPoint.sh").Output()
					// cmd := exec.Command("/bin/sh", "~/work/microbilt_bot/runGetLastPoint.sh")
					out, err := exec.Command("/bin/sh", "runMakeLeft.sh").Output()
					// cmd.Stdout = os.Stdout
					// err := cmd.Run()
					if err != nil {
						// panic(err)
						log.Fatal(err)
					}
					if out != nil {
						fmt.Printf("output is %s\n", out)
					}
					fmt.Printf("Successfull!")
					// fmt.Printf("output is %s\n", cmd.Stdout)
					// m.Text = randomJoke()
					// postMessage(ws, out)
				}(m)
			} else {
				// huh?
				m.Text = fmt.Sprintf("sorry, that does not compute\n")
				postMessage(ws, m)
			}


		} else if m.Type == "message" && m.Channel == "D4H4JBPS6"{
			fmt.Println("Message: ", m.Text)
			//fmt.Println("Chanel: ", m.Channel)
			parts := strings.Fields(m.Text)

			//if len(parts) == 2 && parts[0] == "make" {
			//	if parts[1] == "in" {
			//		fmt.Println("make in")
			//		// cmd := exec.Command("/bin/sh", mongoToCsvSH)÷
			//	} else if parts[1] == "out" {
			//		fmt.Println("make out")
			//	}
			//}

			if len(parts) >= 3 && strings.ToLower(parts[0]) == "get" && strings.ToLower(parts[1]) == "last" && strings.ToLower(parts[2]) == "point" {
				fmt.Println("runGetLastPoint")
				go func(m Message) {
					out, err := exec.Command("/bin/sh", "runGetLastPoint.sh").Output()
					if err != nil {
						// panic(err)
						log.Fatal(err)
					}
					response := parse_response(string(out))
					m.Text = response
					postMessage(ws, m)
				}(m)
			} else if len(parts) >= 2 && strings.ToLower(parts[0]) == "make" && strings.ToLower(parts[1]) == "in" {
				fmt.Println("runMakeArrived")
				go func(m Message) {
					out, err := exec.Command("/bin/sh", "runMakeArrived.sh").Output()
					if err != nil {
						log.Fatal(err)
						return
					}
					r, _ := regexp.Compile(string("<response>([.*])</response>"))
					resp := r.FindAllString(string(out), 1)
					fmt.Printf("output is %s\n", resp)
					if resp != nil {
						m.Text = resp[0]
						postMessage(ws, m)
					}
				}(m)
			} else if len(parts) >= 2 && strings.ToLower(parts[0]) == "make" && strings.ToLower(parts[1]) == "out" {
				// (parts[2] == "out" || parts[2] == "live" || parts[2] == "left") {
				fmt.Println("runMakeLeft")
				go func(m Message) {
					// out, err := exec.Command("sh ~/work/microbilt_bot/runGetLastPoint.sh").Output()
					// cmd := exec.Command("/bin/sh", "~/work/microbilt_bot/runGetLastPoint.sh")
					out, err := exec.Command("/bin/sh", "runMakeLeft.sh").Output()
					// cmd.Stdout = os.Stdout
					// err := cmd.Run()
					if err != nil {
						// panic(err)
						log.Fatal(err)
					}
					if out != nil {
						fmt.Printf("output is %s\n", out)
					}
					fmt.Printf("Successfull!")
					// fmt.Printf("output is %s\n", cmd.Stdout)
					// m.Text = randomJoke()
					// postMessage(ws, out)
				}(m)
			}




		}
	}
}

// Get the quote via Yahoo. You should replace this method to something
// relevant to your team!
func getQuote(sym string) string {
	sym = strings.ToUpper(sym)
	url := fmt.Sprintf("http://download.finance.yahoo.com/d/quotes.csv?s=%s&f=nsl1op&e=.csv", sym)
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Sprintf("error: %v", err)
	}
	rows, err := csv.NewReader(resp.Body).ReadAll()
	if err != nil {
		return fmt.Sprintf("error: %v", err)
	}
	if len(rows) >= 1 && len(rows[0]) == 5 {
		return fmt.Sprintf("%s (%s) is trading at $%s", rows[0][0], rows[0][1], rows[0][2])
	}
	return fmt.Sprintf("unknown response format (symbol was \"%s\")", sym)
}

func parse_response(out string) string {
	r := regexp.MustCompile(string("<response>(.*?)</response>"))
	match := r.FindStringSubmatch(out)
	if match != nil {
		return match[1]

	}
	return "nil"
}
