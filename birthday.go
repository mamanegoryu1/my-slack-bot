package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"
)

func main2() {
	b, err := ioutil.ReadFile("birthday_list.txt")
	if err != nil {
		fmt.Print(err)
		log.Fatal(err)
	}

	// fmt.Println(b)
	str := string(b)

	s := strings.Split(str, "    ")
	// fmt.Println(s[0])
	d := s[0]
	if dateIsNow(d) {
		sendMsg("Attention!")
	}

}

func dateIsNow(d string) bool {
	layout := "2006-01-02"
	d1, _ := time.Parse(layout, d)
	dNow := time.Now()
	fmt.Println(d1)
	fmt.Println(time.Now().Date())
	if d1 == dNow {
		return false
	}
	return true
}

func sendMsg(s string) {
	fmt.Println("Sending message: ...")
}
