#!/usr/bin/env bash
cd /home/roman/work/microbilt_bot
Xvfb :99 -ac -screen 0 1024x768x24
export DISPLAY=:99

#/usr/bin/mvn test -Dtest=testCheckLastPoint > /home/roman/work/microbilt_bot/target/testResultLog.txt
xvfb-run --auto-servernum --server-num=0 nohup /usr/bin/mvn test -Dtest=testCheckLastPoint > /home/roman/work/microbilt_bot/target/testResultLog.txt

